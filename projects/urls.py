from django.urls import path
from projects.views import create_project, get_all_projects, get_one_project


urlpatterns = [
    path("", get_all_projects, name="list_projects"),
    path("<int:id>/", get_one_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
