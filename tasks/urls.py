from django.urls import path
from tasks.views import create_task, get_all_tasks


urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", get_all_tasks, name="show_my_tasks"),
]
